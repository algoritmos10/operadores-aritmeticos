console.log("Hello Wolrd")
//console.log() serve para mostrar mensagens
// texto eu posso usar aspas dulpas, simples ou crase
console.log("=====ATIVIDADE COM OPERADORES ARITMÉTICOS=====")
console.log(2+3-3)
console.log(3+4*5)
console.log(-5+4/3)
console.log(24%5-2)
console.log(Math.sqrt(3+3*2)-1)
console.log(3*2**4)
console.log(8*(8-3%2))
console.log(2.4+3/4)
console.log(10.33-12.2*(3.5/2,4))
console.log("========= FIM ATIVIDADE ========")

console.log(".")

console.log("=====ATIVIDADE COM BOOLEANOS=====");
// Tipos de dados Booleanos
console.log('1. ' + true && false || false);
console.log('2. ' + true && (false || true));
console.log('3. ' + !false && !false);
console.log('4. ' + !(false && false));
console.log('5. ' + false || false(true && true));
console.log('6. ' + !4 < 2 * 3);
console.log('7. ' + 10 > 4 * 3);
console.log('8. ' + 6 + 3 < 9);
console.log('9. ' + ((10 > 4) && (10 > 4 * 3)));
console.log("========= FIM ATIVIDADE ========");

console.log(".")

console.log("========== CONCATENAÇÃO DE STRING ==========")

console.log("Exercício 1: " +50*15+3)
console.log("Meu nome é Milena tenho " + (2021 - 2005) + " anos e moro em Campo Mourão");
console.log("========= FIM ATIVIDADE ========");